from pyspark.sql import SparkSession
from pyspark.sql.functions import input_file_name
from pyspark.sql.types import StructType, StructField
from pyspark.sql.types import DoubleType, IntegerType, StringType
import sys

def triple_subject_query_part():
        triple_subject_query = """SELECT CONCAT('<http://www-iepm.slac.stanford.edu/pinger/lod/resource#Observation-',
                                    monit_site,
                                    '-',
                                    remote_site,
                                    '-',
                                    metric,
                                    '-TIME',
                                    year,
                                    month,
                                    '{0}',
                                    '>"""
        return triple_subject_query

def triple_observation_value(column_index):
    predicate_value = "<http://www-iepm.slac.stanford.edu/pinger/lod/ontology/pinger.owl#hasValue> ', day{0}, ' .') as triple FROM pingerTable WHERE day{0} <> '.'"
    return triple_subject_query_part().format(str(column_index).zfill(2)) + " " + predicate_value.format(column_index)
   
def triple_cube(column_index):
    predicate_value = "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://purl.org/linked-data/cube#Observation> . ') as triple FROM pingerTable WHERE day{0} <> '.'"
    return triple_subject_query_part().format(str(column_index).zfill(2)) + " " + predicate_value.format(column_index)

def triple_dataset(column_index):
    predicate_value = "<http://purl.org/linked-data/cube#dataSet> <http://www-iepm.slac.stanford.edu/pinger/lod/resource#Dataset1> . ') as triple FROM pingerTable WHERE day{0} <> '.'"
    return triple_subject_query_part().format(str(column_index).zfill(2)) + " " + predicate_value.format(column_index)

def triple_source_node(column_index):
    predicate_value = "<http://www.iepm.slac.stanford.edu/pinger/lod/ontology/pinger.owl#hasSourceNode> <http://www-iepm.slac.stanford.edu/pinger/lod/resource#Node-',monit_site,'> .') as triple FROM pingerTable WHERE day{0} <> '.'"
    return triple_subject_query_part().format(str(column_index).zfill(2)) + " " + predicate_value.format(column_index)

def triple_destination_node(column_index):
    predicate_value = "<http://www.iepm.slac.stanford.edu/pinger/lod/ontology/pinger.owl#DestinationNode> <http://www-iepm.slac.stanford.edu/pinger/lod/resource#Node-',remote_site,'> .') as triple FROM pingerTable WHERE day{0} <> '.'"
    return triple_subject_query_part().format(str(column_index).zfill(2)) + " " + predicate_value.format(column_index)

def triple_measure_type(column_index):
    predicate_value = "<http://purl.org/linked-data/cube#measureType> <http://www-iepm.slac.stanford.edu/pinger/lod/resource#Metric',metric,'> .') as triple FROM pingerTable WHERE day{0} <> '.'"
    return triple_subject_query_part().format(str(column_index).zfill(2)) + " " + predicate_value.format(column_index)

def triple_time_uri(column_index):
    predicate_value = "<http://www.iepm.slac.stanford.edu/pinger/lod/ontology/pinger.owl#hasDateTime> <http://www-iepm.slac.stanford.edu/pinger/lod/resource#Time',year,month,'{1}> .') as triple FROM pingerTable WHERE day{0} <> '.'"
    return triple_subject_query_part().format(str(column_index).zfill(2)) + " " + predicate_value.format(column_index, str(column_index).zfill(2))


def main(args):
    input_files = args[1]
    output_files = args[2]
    spark = SparkSession \
        .builder \
        .master("local[12]") \
        .appName("Triplicacao PingeR") \
        .getOrCreate()

    schema = StructType([
        StructField("monit_site", StringType()),
        StructField("remote_site", StringType()),
        StructField("?", StringType()),
        StructField("day1", StringType()),
        StructField("day2", StringType()),
        StructField("day3", StringType()),
        StructField("day4", StringType()),
        StructField("day5", StringType()),
        StructField("day6", StringType()),
        StructField("day7", StringType()),
        StructField("day8", StringType()),
        StructField("day9", StringType()),
        StructField("day10", StringType()),
        StructField("day11", StringType()),
        StructField("day12", StringType()),
        StructField("day13", StringType()),
        StructField("day14", StringType()),
        StructField("day15", StringType()),
        StructField("day16", StringType()),
        StructField("day17", StringType()),
        StructField("day18", StringType()),
        StructField("day19", StringType()),
        StructField("day20", StringType()),
        StructField("day21", StringType()),
        StructField("day22", StringType()),
        StructField("day23", StringType()),
        StructField("day24", StringType()),
        StructField("day25", StringType()),
        StructField("day26", StringType()),
        StructField("day27", StringType()),
        StructField("day28", StringType()),
        StructField("day29", StringType()),
        StructField("day30", StringType()),
        StructField("day31", StringType()),
        StructField("monit_node", StringType()),
        StructField("monit_tld", StringType()),
        StructField("monit_region", StringType()),
        StructField("remote_node", StringType()),
        StructField("remote_tld", StringType()),
        StructField("remote_region", StringType()),
        StructField("metric", StringType()),
        StructField("year", StringType()),
        StructField("month", StringType())
    ])

    #"file:///D:/projeto-final/tcc-get-files-scripts/files/*/2014-jan_2018-aug/*.tsv"
    df = spark \
            .read \
            .schema(schema) \
            .option("header", "false") \
            .option("delimiter", "\t") \
            .csv(input_files) 

    # Remove first line
    df = df.filter(df['monit_site'] != "#Monitoring-Site")

    # Create a view with the data of the files
    df.createOrReplaceTempView("pingerTable")
    
    # Create a empty dataframe to after union
    schema_triple = StructType([StructField("triple", StringType())])   
    total = spark.createDataFrame([],schema_triple)

    # Run the transformation for each day
    for i in range(1,32):
        next_day_value = spark.sql(triple_observation_value(i))
        next_day_cube =  spark.sql(triple_cube(i))
        next_day_dataset = spark.sql(triple_dataset(i))
        next_day_source_node_uri = spark.sql(triple_source_node(i))
        next_day_destination_node_uri = spark.sql(triple_destination_node(i))
        next_day_measure_type = spark.sql(triple_measure_type(i))
        next_day_time_uri = spark.sql(triple_time_uri(i))

        total = total \
                .union(next_day_value) \
                .union(next_day_cube) \
                .union(next_day_dataset) \
                .union(next_day_source_node_uri) \
                .union(next_day_destination_node_uri) \
                .union(next_day_measure_type) \
                .union(next_day_time_uri)

    # 
    total.createOrReplaceTempView("triples")

    #total = spark.sql("select * from triples limit 100000000")

	# Write the resultant dataframe to file
    total.repartition(1).write.mode("overwrite").format("csv").option("quote", None).save("out/{0}/".format(output_files))

if __name__ == "__main__":
    args = sys.argv
    main(args)